import React from "react"
import ReactDom from "react-dom"
import App from "./containers/App/index"
import { BrowserRouter as Router } from "react-router-dom"

ReactDom.render(
    <Router>
        <App />
    </Router>,
    document.getElementById("root")
)